﻿using System;
using System.Threading.Tasks;
using MazeRunner.Web.Components;
using MazeRunner.Web.Helpers;
using MazeRunner.Web.Models;
using Microsoft.AspNetCore.Components;

namespace MazeRunner.Web.Pages
{
    public partial class Index
    {
        private CreateTournamentDialog CreateTournamentDialog { get; set; }
        private bool IsWaiting { get; set; }
        
        [Inject] public NavigationManager NavigationManager { get; set; }

        private void CreateTournament()
        {
            CreateTournamentDialog.Show();
        }

        private async Task CreateTournamentDialog_OnDialogClose()
        {
            IsWaiting = true;
            await CallSomething();

            IsWaiting = false;
            StateHasChanged();
            
            Buffer<TournamentModel>.Add(CreateTournamentDialog.TournamentModel.Id,
                CreateTournamentDialog.TournamentModel);
            NavigationManager.NavigateTo($"Events/{CreateTournamentDialog.TournamentModel.Id}");
        }

        private async Task CallSomething()
        {
            await Task.Delay(5000);
        }
    }
}