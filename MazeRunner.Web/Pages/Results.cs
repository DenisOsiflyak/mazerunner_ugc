﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MazeRunner.Web.Helpers;
using MazeRunner.Web.Models;
using Microsoft.AspNetCore.Components;

namespace MazeRunner.Web.Pages
{
    public partial class Results
    {
        [Parameter] public string CompleteTournamentId { get; set; }
        [Parameter] public string TournamentId { get; set; }
        private TournamentModel TournamentModel { get; set; }
        
        private IEnumerable<GoalsModel> GoalsCount { get; } = new List<GoalsModel>
        {
            new()
            {
                Goals = null
            },
            new()
            {
                Id = Guid.Parse("98b3fad0-0340-4070-aaa8-d76be962a2b4"),
                Goals = 0
            },
            new()
            {
                Id = Guid.Parse("4a83ba10-28ba-4d05-82cd-f38b0b1060ab"),
                Goals = 1
            },
            new()
            {
                Id = Guid.Parse("ceea1c89-31bf-4491-8f59-2f7add0f45ae"),
                Goals = 2
            },
            new()
            {
                Id = Guid.Parse("03f51156-a10f-4970-bc01-72181cfed850"),
                Goals = 3
            },
            new()
            {
                Id = Guid.Parse("1bbc7875-0333-4553-94e0-324c14de5688"),
                Goals = 4
            },
            new()
            {
                Id = Guid.Parse("656c75d4-a435-4fa0-957c-b057c9088864"),
                Goals = 5
            },
            new()
            {
                Id = Guid.Parse("505eb228-d12e-440b-bba4-20071c956143"),
                Goals = 6
            },
            new()
            {
                Id = Guid.Parse("664edff7-8c89-4cc9-b128-c8e452a4757c"),
                Goals = 7
            },
            new()
            {
                Id = Guid.Parse("189f84c6-52e8-43c3-a4d1-fcdc10f8c2d5"),
                Goals = 8
            },
            new()
            {
                Id = Guid.Parse("396f82a0-66db-4d8e-abfc-ecb45f67c4c7"),
                Goals = 9
            },
            new()
            {
                Id = Guid.Parse("95906802-d810-48b6-bf92-80762b8473a6"),
                Goals = 10
            }
        };
        
        protected override void OnInitialized()
        {
            var tournamentResult = Buffer<TournamentModel>.Get(Guid.Parse(TournamentId));
            if (tournamentResult.IsSuccess)
            {
                TournamentModel = tournamentResult.ResultValue;
            }
        }

        private Task CompleteGames()
        {
            return Task.CompletedTask;
        }
    }
}