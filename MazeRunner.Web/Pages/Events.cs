﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MazeRunner.Web.Components;
using MazeRunner.Web.Extensions;
using MazeRunner.Web.Helpers;
using MazeRunner.Web.Models;
using Microsoft.AspNetCore.Components;

namespace MazeRunner.Web.Pages
{
    public partial class Events
    {
        private AddEventsDialog AddEventsDialog { get; set; }
        
        private TournamentModel Tournament { get; set; }
        
        [Parameter] public string TournamentId { get; set; }

        [Parameter] public string CompleteTournamentId { get; set; }
        [Inject] public NavigationManager NavigationManager { get; set; }
        
        private bool IsWaiting { get; set; }

        private void AddEvent()
        {
            AddEventsDialog.Show();
            StateHasChanged();
        }
        
        protected override async Task OnInitializedAsync()
        {
            var tournamentResult = Buffer<TournamentModel>.Get(Guid.Parse(TournamentId));
            if (tournamentResult.IsError)
            {
                return;
            }

            Tournament = tournamentResult.ResultValue;
        }
        
        private async Task AddEventsDialog_OnDialogClose()  
        {
            Tournament.Events ??= new List<EventModel>();
            
            AddEventsDialog.EventModel.TournamentId = Guid.NewGuid();
            AddEventsDialog.EventModel.Id = Guid.NewGuid();
            AddEventsDialog.EventModel.Home = AddEventsDialog.Teams
                .FirstOrDefault(team => team.Id == AddEventsDialog.EventModel.HomeId)
                ?.Name;
            
            AddEventsDialog.EventModel.Away = AddEventsDialog.Teams
                .FirstOrDefault(team => team.Id == AddEventsDialog.EventModel.AwayId)
                ?.Name;
            
            Tournament.Events.Add(AddEventsDialog.EventModel);
        }

        private void DeleteCommand(Guid id)
        {
            var tournamentEventModel = Tournament.Events.FirstOrDefault(@event => @event.Id == id);
            if (tournamentEventModel != null)
            {
                Tournament.Events.Remove(tournamentEventModel);
            }
        }

        private async Task ApplyEvents()
        {
            NavigationManager.NavigateTo($"Tournament/{Tournament.Id}");
        }

        private Task GenerateRandom()
        {
            if (Tournament.History.Any())
            {
                var teams = Tournament.History.Select(history => new[] { history.Home, history.Away }).SelectMany(s => s)
                    .ToHashSet();

                var games = teams.CrossJoin(teams).Where(team => team.Item1 != team.Item2);
                foreach (var game in games)
                {
                    Tournament.Events.Add(new EventModel
                    {
                        Id = Guid.NewGuid(),
                        Home = game.Item1,
                        Away = game.Item2,
                        HomeOdds = 35,
                        DrawOdds = 30,
                        AwayOdds = 35
                    });
                }
            }
            
            return Task.CompletedTask;
        }
    }
}