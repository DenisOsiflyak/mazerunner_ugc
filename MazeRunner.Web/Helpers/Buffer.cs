﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using MazeRunner.Web.Models;

namespace MazeRunner.Web.Helpers
{
    public static class Buffer<TValue>
    {
        private static readonly ConcurrentDictionary<Guid, TValue> TournamentBuffer = new();

        public static Result<TValue> Get(Guid id)
        {
            if (TournamentBuffer.TryGetValue(id, out var val))
            {
                return new Result<TValue>()
                {
                    ResultValue = val
                };
            }

            return new Result<TValue>
            {
                IsError = true
            };
        }

        public static Result<bool> Add(Guid id, TValue val)
        {
            if (TournamentBuffer.TryAdd(id, val))
            {
                return new Result<bool>
                {
                    IsError = false
                };
            }
            
            return new Result<bool>
            {
                IsError = true
            };
        }

        public static Result<bool>  Remove(Guid id)
        {
            if (TournamentBuffer.Remove(id, out _))
            {
                return new Result<bool>
                {
                    IsError = false
                };
            }
            
            return new Result<bool>
            {
                IsError = true
            };
        }
    }
}