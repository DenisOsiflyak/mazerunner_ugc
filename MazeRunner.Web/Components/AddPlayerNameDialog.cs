﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MazeRunner.Web.Models;
using Microsoft.AspNetCore.Components;

namespace MazeRunner.Web.Components
{
    public partial class AddPlayerNameDialog
    {
        public bool ShowDialog { get; set; }
        
        public PlayerModel PlayerModel { get; set; } =
            new();
        
        [Parameter]
        public EventCallback<bool> CloseEventCallback { get; set; }

        public void Show()
        {
            ShowDialog = true;
            StateHasChanged();
        }

        private void Close()
        {
            ShowDialog = false;
            StateHasChanged();
        }
        
        private async Task HandleValidSubmit()
        {
            ShowDialog = false;
            PlayerModel.Id = Guid.NewGuid();

            await CloseEventCallback.InvokeAsync();
            StateHasChanged();
        }

        private async Task AddPlayer()
        {
            StateHasChanged();
            await CloseEventCallback.InvokeAsync();
        }
    }
}