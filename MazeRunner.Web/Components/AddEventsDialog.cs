﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using MazeRunner.Web.Models;
using Microsoft.AspNetCore.Components;

namespace MazeRunner.Web.Components
{
    public partial class AddEventsDialog
    {
        public IEnumerable<TeamModel> Teams { get; set; }
        public EventModel EventModel { get; set; } =
            new();

        private bool ShowDialog { get; set; }
        
        [Parameter]
        public EventCallback<bool> CloseEventCallback { get; set; }
        
        protected override async Task OnInitializedAsync()
        {
            Teams = await Task.FromResult(new List<TeamModel>()
            {
                new()
                {
                    Id = Guid.Parse("25275eeb-0a4f-47da-9f79-8bef4c84ccc9"),
                    Name = "Dynamo Kyiv"
                },
                new()
                {
                    Id = Guid.Parse("25275eeb-0a4f-47da-9f79-8bef4c84ccc8"),
                    Name = "Shakhtart Donetsk"
                }
            });
        }

        
        public void Show()
        {
            EventModel = new EventModel();
            ShowDialog = true;
            StateHasChanged();
        }

        private async Task Close()
        {
            ShowDialog = false;
            await CloseEventCallback.InvokeAsync();
            StateHasChanged();
        }

        private async Task HandleValidSubmit()
        {
        }

        private void OnInputFileChange()
        {
            StateHasChanged();
        }
        
        private async Task AddForm()
        {
            ShowDialog = false;
            StateHasChanged();
            await CloseEventCallback.InvokeAsync();
        }
    }
}