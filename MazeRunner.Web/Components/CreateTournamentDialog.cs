﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using MazeRunner.Web.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;

namespace MazeRunner.Web.Components
{
    public partial class CreateTournamentDialog
    {
        private bool IsWaiting { get; set; }
        
        private List<IBrowserFile> _history = new();
        
        public TournamentModel TournamentModel { get; set; } =
            new();

        private IEnumerable<SportModel> Sports { get; set; } = new List<SportModel>()
        {
            new ()
            {
                Id = Guid.NewGuid(),
                Sport = "Cyberfootball"
            },
            new ()
            {
                Id = Guid.NewGuid(),
                Sport = "Football"
            }
        };
        private bool ShowDialog { get; set; }
        
        [Parameter]
        public EventCallback<bool> CloseEventCallback { get; set; }
        
        public void Show()
        {
            ShowDialog = true;
            StateHasChanged();
        }

        private void Close()
        {
            ShowDialog = false;
            StateHasChanged();
        }

        private async Task HandleValidSubmit()
        {
            ShowDialog = false;
            TournamentModel.Id = Guid.NewGuid();
            TournamentModel.CompleteTournamentId = Guid.NewGuid();
            TournamentModel.Events = new List<EventModel>();

            TournamentModel.History ??= new List<History>();
            
            await CloseEventCallback.InvokeAsync();
            StateHasChanged();
        }

        private async Task OnInputFileChange(InputFileChangeEventArgs e)
        {
            IsWaiting = true;
            Regex regex = new Regex(".+\\.csv", RegexOptions.Compiled);  
            if (!regex.IsMatch(e.File.Name))
            {
                throw new ArgumentException("File is incorrect");
            }

            var stream = e.File.OpenReadStream();  
            var csv = new List<History>();
            if (csv == null) throw new ArgumentNullException(nameof(csv));
            var ms = new MemoryStream();  
            await stream.CopyToAsync(ms);  
            stream.Close();  
            var outputFileString = System.Text.Encoding.UTF8.GetString(ms.ToArray());
            csv.AddRange(outputFileString.Split(Environment.NewLine).Select(SplitCsv).SelectMany(s => s));
            TournamentModel.History = csv.Where(history => history != null).ToList();
            IsWaiting = false;
        }
        
        private static List<History> SplitCsv(string input)  
        {
            var delimiters = new[] { ',', ';' };
            char? del = null;
            foreach (var delimiter in delimiters)
            {
                if (input.Contains(delimiter))
                {
                    del = delimiter;
                } 
            }

            if (del == null)
            {
                return new List<History>();
            }
            
            
            var list = new List<History>();
            var game = input.Split((char)del);
            try
            {
                list.Add(new History()
                {
                    Home = game[0],
                    Away = game[1],
                    HomeScore = int.Parse(game[2]),
                    AwayScore = int.Parse(game[3]),
                    GameOrder = int.Parse(game[4])
                });
            }
            catch (Exception)
            {
                // ignored
            }
  
            return list;  
        }  
    }
}