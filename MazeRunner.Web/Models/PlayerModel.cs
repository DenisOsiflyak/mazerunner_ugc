﻿using System;
using System.ComponentModel.DataAnnotations;

namespace MazeRunner.Web.Models
{
    public class PlayerModel
    {
        public Guid Id { get; set; }
        
        [Required]
        public string Name { get; set; }
    }
}