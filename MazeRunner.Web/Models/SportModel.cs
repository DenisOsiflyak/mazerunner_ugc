﻿using System;

namespace MazeRunner.Web.Models
{
    public class SportModel
    {
        public Guid Id { get; set; }
        public string Sport { get; set; }
    }
}