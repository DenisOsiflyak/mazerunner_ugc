﻿namespace MazeRunner.Web.Models
{
    public class Result
    {
        public bool IsError { get; set; }
        public bool IsSuccess => !IsError;
    }
    
    public class Result<TResult> : Result
    {
        public TResult ResultValue { get; set; }
    }
}