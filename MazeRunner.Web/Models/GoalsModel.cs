﻿using System;

namespace MazeRunner.Web.Models
{
    public class GoalsModel
    {
        public Guid Id { get; set; }
        public int? Goals { get; set; }
    }
}