﻿using System;

namespace MazeRunner.Web.Models
{
    public class EventModel
    {
        public Guid Id { get; set; }
        public Guid TournamentId { get; set; }
        public Guid HomeId { get; set; }
        public string Home { get; set; }
        public Guid AwayId { get; set; }
        public string Away { get; set; }
        public int? HomeGoal { get; set; }
        public int? AwayGoal { get; set; }
        public Guid AwayGoalId { get; set; }
        public Guid HomeGoalId { get; set; }
        public GameResult Winner { get; set; }
        public double HomeOdds { get; set; }
        public double AwayOdds { get; set; }
        public double DrawOdds { get; set; }
    }

    public enum GameResult
    {
        None, HomeWin, AwayWin, Draw
    }
}