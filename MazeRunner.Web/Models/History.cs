﻿namespace MazeRunner.Web.Models
{
    public class History
    {
        public int GameOrder { get; set; }
        public string Home { get; set; }
        public int HomeScore { get; set; }
        public string Away { get; set; }
        public int AwayScore { get; set; }
    }
}