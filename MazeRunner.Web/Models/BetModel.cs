﻿using System;

namespace MazeRunner.Web.Models
{
    public class BetModel
    {
        public Guid Id { get; set; }
        public Guid EventId { get; set; }
        public GameResult Winner { get; set; }
        public int? HomeScore { get; set; }
        public int? AwayScore { get; set; }
    }
}