﻿using System;

namespace MazeRunner.Web.Models
{
    public class TeamModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}