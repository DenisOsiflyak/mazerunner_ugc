﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MazeRunner.Web.Models
{
    public class TournamentModel
    {
        public Guid Id { get; set; }
        public Guid CompleteTournamentId { get; set; }
        
        [Required]
        public string Tournament { get; set; }
        
        [Required]
        [EmailAddress]
        public string OwnerEmail { get; set; }
        
        [Required]
        public Guid SportId { get; set; }
        public string PlayerName { get; set; }
        public List<EventModel> Events { get; set; }
        public List<History> History { get; set; }

        public List<BetModel> Bets { get; set; }
    }
}