﻿namespace MazeRunner.Domain.Models
{
    public enum SportType
    {
        Football
    }

    public enum MatchWinnerBetType
    {
        HomeWin, AwayWin, Draw
    }

    public enum MatchStatus
    {
        NotStarted, Live, FullTime
    }
}