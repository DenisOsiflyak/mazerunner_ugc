﻿using System.Threading.Tasks;
using MazeRunner.Domain.Models;
using Microsoft.AspNetCore.Mvc;

namespace MazeRunner.Api.Controllers
{
    [ApiController]
    [Route("event")]
    public class EventController : ControllerBase
    {
        [HttpPost("add")]
        public async Task<IActionResult> AddEvent(TournamentViewModel tournamentViewModel)
        {
            return Ok();
        }
        
        [HttpPost("complete")]
        public async Task<IActionResult> CompleteEvent(CompleteEventViewModel completeEventViewModel)
        {
            return Ok();
        }
    }
}