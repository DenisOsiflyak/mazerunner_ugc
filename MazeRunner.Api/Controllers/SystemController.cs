﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace MazeRunner.Api.Controllers
{
    [ApiController]
    [Route("system")]
    public class SystemController : ControllerBase
    {
        [HttpGet("odds")]
        public async Task<IActionResult> GetOdds(Guid eventId)
        {
            return Ok();
        }
        
        [HttpGet("sports")]
        public async Task<IActionResult> GetAvailableSport()
        {
            return Ok();
        }

        [HttpGet("statistic")]
        public async Task<IActionResult> GetUserBetStatistic()
        {
            return Ok();
        }
    }
}