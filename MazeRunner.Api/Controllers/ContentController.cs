﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MazeRunner.Api.Controllers
{
    [ApiController]
    [Route("content")]
    public class ContentController : ControllerBase
    {
        [HttpPost("history")]
        public async Task<IActionResult> ImportHistoricalData(List<IFormFile> history)
        {
            return Ok();
        }
    }
}