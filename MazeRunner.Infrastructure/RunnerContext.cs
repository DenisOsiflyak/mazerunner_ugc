﻿using MazeRunner.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;

namespace MazeRunner.Infrastructure
{
    public class RunnerContext : DbContext, IRunnerContext
    {
        public DbSet<ContentEntity> Contents { get; set; }
        public DbSet<EventEntity> Events { get; set; }
        public DbSet<MatchWinnerBetEntity> MatchWinnerBets { get; set; }
        public DbSet<MatchWinnerOddEntity> MatchWinnerOdds { get; set; }
        public DbSet<ScoreBetEntity> ScoreBets { get; set; }
        public DbSet<TeamEntity> Teams { get; set; }
        public DbSet<UserEntity> Users { get; set; }
        
        public RunnerContext(DbContextOptions<RunnerContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ContentEntity>()
                .HasOne(entity => entity.User)
                .WithMany(entity => entity.Contents)
                .HasForeignKey(key => key.UserId);

            modelBuilder.Entity<EventEntity>()
                .HasOne(entity => entity.Content)
                .WithMany(entity => entity.Events)
                .HasForeignKey(key => key.ContentId);
            
            modelBuilder.Entity<EventEntity>()
                .HasOne(entity => entity.Team)
                .WithMany(entity => entity.Events)
                .HasForeignKey(key => key.HomeId);
            
            modelBuilder.Entity<EventEntity>()
                .HasOne(entity => entity.Team)
                .WithMany(entity => entity.Events)
                .HasForeignKey(key => key.AwayId);
            
            modelBuilder.Entity<EventEntity>()
                .HasOne(entity => entity.User)
                .WithMany(entity => entity.Events)
                .HasForeignKey(key => key.UserId);
            
            modelBuilder.Entity<EventEntity>()
                .HasOne(entity => entity.MatchWinnerOdd)
                .WithMany(entity => entity.Events)
                .HasForeignKey(key => key.MatchWinnerOddId);
            
            modelBuilder.Entity<MatchWinnerBetEntity>()
                .HasOne(entity => entity.User)
                .WithMany(entity => entity.MatchWinnerBets)
                .HasForeignKey(key => key.UserId);
            
            modelBuilder.Entity<MatchWinnerBetEntity>()
                .HasOne(entity => entity.Event)
                .WithMany(entity => entity.MatchWinnerBets)
                .HasForeignKey(key => key.EventId);
            
            modelBuilder.Entity<ScoreBetEntity>()
                .HasOne(entity => entity.Event)
                .WithMany(entity => entity.ScoreBets)
                .HasForeignKey(key => key.EventId);
            
            modelBuilder.Entity<ScoreBetEntity>()
                .HasOne(entity => entity.User)
                .WithMany(entity => entity.ScoreBets)
                .HasForeignKey(key => key.UserId);
        }
    }
}