﻿using System;
using System.Collections.Generic;

namespace MazeRunner.Infrastructure.Entities
{
    public class MatchWinnerOddEntity
    {
        public Guid Id { get; set; }
        public double Home { get; set; }
        public double Away { get; set; }
        public double Draw { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime UpdateAt { get; set; }
        
        public ICollection<EventEntity> Events { get; set; }
    }
}