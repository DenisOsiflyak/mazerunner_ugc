﻿using System;
using System.Collections.Generic;

namespace MazeRunner.Infrastructure.Entities
{
    public class EventEntity
    {
        public Guid Id { get; set; }
        public Guid ContentId { get; set; }
        public Guid UserId { get; set; }
        public Guid HomeId { get; set; }
        public Guid AwayId { get; set; }
        public Guid MatchWinnerOddId { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime UpdateAt { get; set; }
        
        public ContentEntity Content { get; set; }
        public UserEntity User { get; set; }
        public TeamEntity Team { get; set; }
        public MatchWinnerOddEntity MatchWinnerOdd { get; set; }
        public ICollection<MatchWinnerBetEntity> MatchWinnerBets { get; set; }
        public ICollection<ScoreBetEntity> ScoreBets { get; set; }
    }
}