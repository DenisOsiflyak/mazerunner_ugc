﻿using System;
using System.Collections.Generic;

namespace MazeRunner.Infrastructure.Entities
{
    public class TeamEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime UpdateAt { get; set; }

        public ICollection<EventEntity> Events { get; set; }
    }
}