﻿using System;

namespace MazeRunner.Infrastructure.Entities
{
    public class MatchWinnerBetEntity
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid EventId { get; set; }
        public int MatchWinnerBetType { get; set; }
        public string MatchWinnerBetName { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime UpdateAt { get; set; }
        
        public UserEntity User { get; set; }
        public EventEntity Event { get; set; }
    }
}