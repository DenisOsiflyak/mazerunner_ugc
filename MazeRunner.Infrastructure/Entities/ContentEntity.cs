﻿using System;
using System.Collections.Generic;

namespace MazeRunner.Infrastructure.Entities
{
    public class ContentEntity
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public int SportType { get; set; }
        public string SportName { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime UpdateAt { get; set; }
        
        public UserEntity User { get; set; }
        public ICollection<EventEntity> Events { get; set; }
    }
}