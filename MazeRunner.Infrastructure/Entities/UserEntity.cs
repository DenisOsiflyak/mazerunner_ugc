﻿using System;
using System.Collections.Generic;

namespace MazeRunner.Infrastructure.Entities
{
    public class UserEntity
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime UpdateAt { get; set; }
        
        public ICollection<ContentEntity> Contents { get; set; }
        public ICollection<EventEntity> Events { get; set; }
        public ICollection<MatchWinnerBetEntity> MatchWinnerBets { get; set; }
        public ICollection<ScoreBetEntity> ScoreBets { get; set; }
    }
}