﻿using System.Threading;
using System.Threading.Tasks;
using MazeRunner.Infrastructure.Entities;
using Microsoft.EntityFrameworkCore;

namespace MazeRunner.Infrastructure
{
    public interface IRunnerContext
    {
        DbSet<ContentEntity> Contents { get; set; }
        DbSet<EventEntity> Events { get; set; }
        DbSet<MatchWinnerBetEntity> MatchWinnerBets { get; set; }
        DbSet<MatchWinnerOddEntity> MatchWinnerOdds { get; set; }
        DbSet<ScoreBetEntity> ScoreBets { get; set; }
        DbSet<TeamEntity> Teams { get; set; }
        DbSet<UserEntity> Users { get; set; }
        int SaveChanges();
        Task<int> SaveChangesAsync(CancellationToken cancellationToken);
    }
}